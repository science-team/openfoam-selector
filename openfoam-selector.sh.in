# -*- sh -*-
# Copyright (c) 2007 Cisco Systems, Inc.  All rights reserved.
# Copyright (C) 2018-2019 OpenCFD Ltd.
#
# File installed for Bourne-shell startups to select which OPENFOAM
# installation to use.  Not using "alternatives" because we want to be
# able to set per-user level defaults, not just system-wide defaults.
#
# Would be nice to define select mechanism as a function for later reuse
# but that squashes aliases

openfoam_selector_dir="@OPENFOAM_SELECTOR_DATADIR@"
openfoam_selector_homefile="$HOME/@OPENFOAM_SELECTOR_HOME_FILE@"
openfoam_selector_sysfile="@OPENFOAM_SELECTOR_SYSCONFDIR@/@OPENFOAM_SELECTOR_SYSCONFIG_FILE@"

# The selection name
unset openfoam_selection
if [ -f "$openfoam_selector_homefile" ]
then
    openfoam_selection=$(cat "$openfoam_selector_homefile")
elif [ -f "$openfoam_selector_sysfile" ]
then
    openfoam_selection=$(cat "$openfoam_selector_sysfile")
fi

if [ -n "$openfoam_selection" ] && \
   [ -f "$openfoam_selector_dir/$openfoam_selection" ]
then
    # Resolve to OpenFOAM directory
    openfoam_selection=$(cat "$openfoam_selector_dir/$openfoam_selection")

    if [ -n "$openfoam_selection" ] && [ -f "$openfoam_selection/etc/bashrc" ]
    then
        . "$openfoam_selection/etc/bashrc" ''
    fi
fi

# Cleanup
unset openfoam_selection openfoam_selector_dir
unset openfoam_selector_homefile openfoam_selector_sysfile

# -----------------------------------------------------------------------------
